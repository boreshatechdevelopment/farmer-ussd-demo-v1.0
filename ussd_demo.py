"""
For Demo purposes only! To be copied into Cloud Functions inline editor...
Author: James T

NOTE: Only works for Cash Advances option...
"""
import requests

def ussd_callback(request):
    global response
    session_id = request.values.get('sessionId',None)
    service_code = request.values.get('serviceCode',None)
    phone_number = request.values.get('phoneNumber',None)
    text = request.values.get('text','default')

    fee_rate = float(2.5)/100

    if text == '' :
        response = 'CON '
        response += 'Welcome Miria Karema! What can we help you with? \n'
        response += '1. My account \n'
        response += '2. Cash advance \n'
        response += '3. Other products'

    elif text == '2' :
        response = 'CON '
        response += 'You can take up to 750,000 UGX in cash advance. How much would you like to receive? \n'

    elif text[0] == '2' and text.count('*') == 1:
        request_amt = int( text[2:] )
        fee = int( request_amt * fee_rate )
        request_amt = f'{request_amt:,}' # format as string with comma separators
        fee = f'{fee:,}' # format as string with comma separators
        response = 'CON '
        response += 'You have requested %s UGX cash advance. The fee is %s UGX. Accept? \n' % (request_amt, fee)
        response += '1. Yes \n'
        response += '2. No'

    elif text[0] == '2' and text.count('*') == 2 and text[-1] == '1':
        response = 'END '
        response += 'Thank you. Your request has been received by Kiboga Dairy Cooperative Society and will be approved shortly.'
    
    elif text[0] == '2' and text.count('*') == 2 and text[-1] == '2':
        response = 'END '
        response += 'Your request has been CANCELLED.'

    return response